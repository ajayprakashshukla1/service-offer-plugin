<script type="text/javascript">
    jQuery(document).ready(function (){
        
        jQuery('#phone-input').keypress(function(e){
          
            if ((e.which != 8 && e.which != 0 && e.which != 43 && (e.which < 48 || e.which > 57)) || e.which == 95) {
             return false;
          }
    }); 
        
        
        
 jQuery('#wrarranty-offer').submit(function(){
      jQuery('.validation_error').html('<p class="err"></p>');
      
      var email = jQuery('#email-input').val(); 

           if(email == ''){
             jQuery('.validation_error').html('<li class="err">Please Enter Email</li>');
             return false;
         }
     
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(email) == false) 
        {
           jQuery('.validation_error').append('<li class="err">Please enter a valid email address. </li>');
            return false;
        }
        
        jQuery("#modal-overlay").show();
         var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
         var subject = 'Warranty';
         var $form = jQuery(this);
           jQuery.ajax({
            url : ajaxurl,
            type : 'post',
            data : {
                action : 'send_offer_detail_mail',
                data : $form.serialize(),
                subject: subject
               
            },
            success : function( response ) {
               if(response == '1'){
                  
                   jQuery('.mail_success').text('Enquiry sumbitted Successfully');
                   document.getElementById("wrarranty-offer").reset();

               }else{
                   jQuery('.mail_success').text('Something Went Wrong Please try again');
               }
                jQuery("#modal-overlay").hide();
            }
        });
        
        
        return false;
     
 });
 });
 


    </script>
 <style>
        .err{
            color: red;
        }
        #modal-overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(f,f,f,0.5);
    z-index: 2;
    cursor: pointer;
}
   
 #text{
    position: absolute;
    top: 50%;
    left: 50%;
    font-size: 50px;
    
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
}  
    </style>
<form id="wrarranty-offer" action="" method="post">
<div class="h-rule-dot"></div>
<h5 class="mail_success">
    <?php
    if(isset($success_message)){
        echo $success_message;
    }
    
    ?>
    
</h5>
<h5>
    <?php
  
    if(isset($error_message)){
        echo $error_message;
    }
    
    ?>
    
</h5>
<div class="validation_error" stye="color:red;">
                       
		</div>
Please complete the form below.


         
 <input type="hidden" name="service_type" value="warranty" />
<div class="input-form form-warranty">
<div class="form-groups row">
<div class="form-group col-md-6 col-xs-12">

<label for="homePhone">First Name</label>
<div class="input-group">
<div class="input-group-addon"></div>
<input id="first-name" name="first_name" class="form-control form-control-lg" type="text" placeholder="John" required />

</div>
</div>
<div class="form-group col-md-6 col-xs-12">

<label for="homePhone">Last Name</label>
<div class="input-group mb-2">
<div class="input-group-addon"></div>
<input id="last-name" name="last_name" class="form-control form-control-lg" type="text" placeholder="Doe" required />

</div>
</div>
<div class="form-group col-md-6 col-xs-12">

<label for="homePhone">Email address</label>
<div class="input-group">
<div class="input-group-addon"></div>
<input id="email-input" name="email_address" class="form-control form-control-lg" type="text" placeholder="john@example.com" required />

</div>
</div>
<div class="form-group col-md-6 col-xs-12">

<label for="homePhone">Phone Number</label>
<div class="input-group">
<div class="input-group-addon"></div>
<input id="phone-input" name="phone_number" class="form-control form-control-lg" type="text" placeholder="+123456789" data-input="phone" required />

</div>
</div>
<div class="col-sm-12"><label for="homePhone">Warranty term interested in</label></div>
<div class="form-group col-sm-2">
    <div class="checkbox"><label><input name="warranty" value="2 Years" type="radio"  required/> 2 year Warranty </label></div>
</div>
<div class="form-group col-sm-2">
<div class="checkbox"><label> <input  name="warranty" value="3 Years"type="radio" required /> 3 years Warranty </label></div>
</div>
<div class="form-group col-sm-2">
<div class="checkbox"><label> <input name="warranty" value="5 Years" type="radio" required /> 5 years Warranty </label></div>
</div>
<div class="form-group col-md-12">
    <button class="btn btn-primary btn-send" name="warranty_form" type="submit">Send</button></div>
</div>
</div>
 </form>
    <div id="modal-overlay">
    <img id="text" src="<?php echo plugin_dir_url( __FILE__);  ?>img/spiffygif_32x32.gif" />
</div>

