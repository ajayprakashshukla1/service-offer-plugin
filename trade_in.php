 <script type="text/javascript">
    jQuery(document).ready(function (){
        
        jQuery('#mobilePhone').keypress(function(e){
       
            if ((e.which != 8 && e.which != 0 && e.which != 43 && (e.which < 48 || e.which > 57)) || e.which == 95) {
             return false;
          }
    }); 
     
    jQuery('#trade_in_offer').submit(function(){
   jQuery("#modal-overlay").show();
     var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
         var subject = 'Trade In';
         var $form = jQuery(this);
           jQuery.ajax({
            url : ajaxurl,
            type : 'post',
            data : {
                action : 'send_offer_detail_mail',
                data : $form.serialize(),
                subject: subject
               
            },
            success : function( response ) {
               if(response == '1'){
                   
                   jQuery('.mail_success').text('Enquiry sumbitted Successfully');
                    document.getElementById("trade_in_offer").reset();
               }else{
                   jQuery('.mail_success').text('Something Went Wrong Please try again');
               }
               jQuery("#modal-overlay").hide();
            }
        });
        
        
        return false;
 
   });
 });
 


    </script>




<style>
        .err{
            color: red;
        }
        #modal-overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(f,f,f,0.5);
    z-index: 2;
    cursor: pointer;
}
   
 #text{
    position: absolute;
    top: 50%;
    left: 50%;
    font-size: 50px;
    
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
}  
    </style>
<form id="trade_in_offer" action="#" method="post">
<div class="featurebox form-bg-1">
<div class="row">
<div class="col-md-12">
 <input type="hidden" name="service_type" value="trade_in" />
<div class="h-rule-dot"></div>
</div>
<div class="col-md-7 col-sm-12">
<h4>A trade-in service that's simple, hassle-free and makes you the winner.</h4>
So you've found a vehicle you would like to buy but can't because you haven't sold or dealt with your current vehicle. That dilemma is now a thing of the past. Pickles is proud to offer an innovative auction used car trade-in Agreement.

You will now be able to buy your next vehicle with our used car trade-in service knowing exactly what your current vehicle is worth, together with the convenience of not being without transport for any period of time.
<div class="steps-wrapper">
<div class="step-icon"></div>
<h4>Step 1</h4>
Simply bring in your current vehicle for an inspection by one of our expert valuers. They will inspect the vehicle and issue you with Pickles used car trade-in certificate. This certificate will clearly show the net trade-in value of that vehicle. A small fee applies for this certificate.

</div>
<div class="steps-wrapper">
<div class="step-icon"></div>
<h4>Step 2</h4>
You may bid on any car at any Pickles Auction in Australia using your trade-in certificate. Simply bid on the selected car, pay the normal deposit and show your trade-in certificate to the cashier at time of payment.

</div>
<div class="steps-wrapper">
<div class="step-icon"></div>
<h4>Step 3</h4>
The net trade-in value as per your used car trade-in certificate will be immediately credited to the invoice of your auction purchase. All you do is pay the difference between the net trade-in value and the auction price. If you are downtrading, Pickles will give you back the difference.

</div>
<div class="steps-wrapper">
<div class="step-icon"></div>
<h4>Step 4</h4>
Bring in your car trade-in within 24 hours of the auction you attended, get it checked by our staff and after paying your account, drive out with your latest purchase. Should you not have time to get a roadworthy certificate, we can arrange one for you. There is no need to be without a car.

</div>
Whilst we hope you will, you are not bound to buy your replacement vehicle from Pickles Auctions. You may use the used car trade-in service to dispose of your existing vehicle through Pickles and then buy your replacement from someone else. Again, the choice is yours. The Pickles used car trade-in certificate is valid for 21 days and conditions do apply.

</div>
<div class="col-md-5 col-sm-12">
<div class="row">
    <div class="form-group col-md-12">
        <h5 class="mail_success"><?php if(isset($success_message)){echo $success_message;} ?>  </h5>
<h5><?php if(isset($error_message)){ echo $error_message; } ?></h5>
    <!-- our error container -->
		<div class="container" stye="display:none">
                        <ul style="color:red">	
			</ul>
		</div>
   </div> 
<div class="form-group col-md-12"><label for="firstName">First Name<span class="required-field-marker">*</span></label> <input id="firstName" class="form-control" maxlength="75" name="first_name" type="text" value="" placeholder="John" required /></div>
<div class="form-group col-md-12"><label for="lastName">Last Name<span class="required-field-marker">*</span></label> <input id="lastName" class="form-control" maxlength="75" name="last_name" type="text" value="" placeholder="Doe" required /></div>
<div class="form-group col-md-12"><label for="mobilePhone">Mobile Phone</label> <input id="mobilePhone" class="form-control" maxlength="18" name="mobile_phone" type="text" value="" placeholder="+123456789" /></div>
<div class="form-group col-md-6"><label for="make">Make*</label> <input id="make" class="form-control" name="make" type="text" value="" placeholder="BMW" required /></div>
<div class="form-group col-md-6"><label for="model">Model*</label> <input id="model" class="form-control" name="model" type="text" value="" placeholder="X5" required /></div>
<div class="form-group col-md-6"><label for="year">Year*</label> <input id="year" class="form-control" name="year" type="text" value="" placeholder="2017" required /></div>
<div class="form-group col-md-6"><label for="vin">VIN</label> <input id="vin" class="form-control" name="vin" type="text" value="" placeholder="BMW-X-12345" /></div>
<div class="form-group col-md-12"><label for="engine">Engine</label> <input id="engine" class="form-control" name="engine" type="text" value="" placeholder="4.0 Ltr Diesel" /></div>
<div class="form-group col-md-12"><label for="vehicle-descrip">Quick Description of Your Vehicle *</label> <input id="vehicle-descrip" class="form-control" name="vehicle_descrip" type="text" value="" placeholder="Type Description here..." required /></div>
<div class="form-group col-md-12"><label for="location">Location*</label>
<div class="select-input-arrow"><select id="state" class="form-control" name="state" required>
<option selected="selected" value="">choose</option>
<option value="Australian Capital Territory">Australian Capital Territory</option>
<option value="New South Wales">New South Wales</option>
<option value="Northern Territory">Northern Territory</option>
<option value="Queensland">Queensland</option>
<option value="South Australia">South Australia</option>
<option value="Tasmania">Tasmania</option>
<option value="Victoria">Victoria</option>
<option value="Western Australia">Western Australia</option>
</select></div>
</div>
<div class="form-group col-md-12 text-center">
    <button class="btn btn-primary btn-send" name="trade_in" type="submit">Send</button></div>
</div>
</div>
</div>
</div>
</form>
    <div id="modal-overlay">
    <img id="text" src="<?php echo plugin_dir_url( __FILE__);  ?>img/spiffygif_32x32.gif" />
</div>