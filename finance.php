<script type="text/javascript">
    jQuery(document).ready(function (){
        
       jQuery('#homePhone,#workPhone,#mobilePhone').keypress(function(e){
          
           if ((e.which != 8 && e.which != 0 && e.which != 43 && (e.which < 48 || e.which > 57)) || e.which == 95) {
             return false;
          }
    });  
        
        
        
        
        
//        jQuery('#contactMethod').change(function(){
//          jQuery('#mobilePhone').val('');  
//          jQuery('#workPhone').val('');
//          jQuery('#emailAddress').val(''); 
//          jQuery('#homePhone').val('');
//          jQuery('#confirmEmailAddress').val('');
//        });
        
        
        jQuery('#postcode').keypress(function(e){
           
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
             return false;
          }
    });
 
        
       
        
        
        jQuery('#finance_offer').submit(function(){
            jQuery('.validation_error').html('<p class="err"></p>');
            
            
        var mobilePhone =  jQuery('#mobilePhone').val();  
        var workPhone =  jQuery('#workPhone').val();
        var homePhone =   jQuery('#homePhone').val();
            
            if(mobilePhone =="" && workPhone == "" &&  homePhone ==""){
            jQuery('.validation_error').append('<li class="err">Please enter at least one phone number home, work or mobile </li>');
            return false;
            }
        
        
            var email = jQuery('#emailAddress').val(); 
        var confemail = jQuery('#confirmEmailAddress').val();
        
        
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(email) == false) 
        {
           jQuery('.validation_error').append('<li class="err">Please enter a valid email address. </li>');
            return false;
        }
        
        if(confemail ==''){
            
              jQuery('.validation_error').append('<li class="err">Please enter a  Confirm email address. </li>');
            return false;
                }
        
        
        if (reg.test(confemail) == false) 
        {
           jQuery('.validation_error').append('<li class="err">Please enter a valid email address. </li>');
            return false;
        }
        
        if(email != confemail) {
            jQuery('.validation_error').append('<li class="err">Email Address and Confrim Email Address Should Match </li>');
            return false;
        }
        
        
        
        
        
        
            var contactmethod = jQuery('#contactMethod').val();
           
            switch(contactmethod) {
                
         case 'Mobile Phone':
         var mobilephone =  jQuery('#mobilePhone').val();
         if(mobilephone == ''){
             jQuery('.validation_error').append('<li class="err">Please Enter Mobile Phone as You have selected as Contact Prefrence </li>');
             return false;
         }
        
        break;
        case 'Work Phone':
          
        var workphone = jQuery('#workPhone').val();
         
       if(workphone == ''){
             jQuery('.validation_error').append('<li class="err">Please Enter Work Phone as You have selected as Contact Prefrence </li>');
             return false;
         }
        break;
    case 'Home Phone':
    var homephone = jQuery('#homePhone').val();
      
    if(homephone == ''){
             jQuery('.validation_error').append('<li class="err">Please Enter Home Phone as You have selected as Contact Prefrence </li>');
             return false;
         }
    break;
    case 'Email':
    
          var email = jQuery('#emailAddress').val();
          
           if(email == ''){
             jQuery('.validation_error').append('<li class="err">Please Enter Email as You have selected as Contact Prefrence </li>');
             return false;
         }else{
             var email = jQuery('#emailAddress').val(); 
        var confemail = jQuery('#confirmEmailAddress').val();
        
        
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(email) == false) 
        {
           jQuery('.validation_error').append('<li class="err">Please enter a valid email address. </li>');
            return false;
        }
        
        if(confemail ==''){
            
              jQuery('.validation_error').append('<li class="err">Please enter a  Confirm email address. </li>');
            return false;
                }
        
        
        if (reg.test(confemail) == false) 
        {
           jQuery('.validation_error').append('<li class="err">Please enter a valid email address. </li>');
            return false;
        }
        
        if(email != confemail) {
            jQuery('.validation_error').append('<li class="err">Email Address and Confrim Email Address Should Match </li>');
            return false;
        }
       
        
            }
    break;
    default:
        
       
       return true;
      }
      
       jQuery("#modal-overlay").show();
      var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
         var subject = 'Finance';
         var $form = jQuery(this);
           jQuery.ajax({
            url : ajaxurl,
            type : 'post',
            data : {
                action : 'send_offer_detail_mail',
                data : $form.serialize(),
                subject: subject
               
            },
            success : function( response ) {
               if(response == '1'){
                  
                   jQuery('.mail_success').text('Enquiry sumbitted Successfully');
                   document.getElementById("finance_offer").reset();
               }else{
                   jQuery('.mail_success').text('Something Went Wrong Please try again');
               }
                jQuery("#modal-overlay").hide();
            }
        });
        
        
        return false;
      
      
      
        });
        
    });
    </script>
    
    <style>
        .err{
            color: red;
        }
        #modal-overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(f,f,f,0.5);
    z-index: 2;
    cursor: pointer;
}
   
 #text{
    position: absolute;
    top: 50%;
    left: 50%;
    font-size: 50px;
    
    transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
}  
    </style>
    
<form id="finance_offer" action="" method="post">
<div class="featurebox form-bg-1">
<div class="row">
<div class="col-md-12">
    <h5 class="mail_success"><?php if(isset($success_message)){echo $success_message; } ?>  </h5>
<h5><?php if(isset($error_message)){ echo $error_message; } ?></h5>
<div class="h-rule-dot"></div>
To contact a Financial Services Customer Officer about securing Finance for your next purchase, please complete the form below.
<span class="required-field-marker">*</span> fields are mandatory

</div>
   <div class="col-md-12">
    <!-- our error container -->
		<div class="validation_error" stye="color:red;">
                       
		</div>
</div> 
</div>
<div class="row">
<fieldset class="margin-top-none col-md-6 your-details-enquiry-form">
    
<h4>Your Details</h4>

<div class="row">
<div class="form-group col-md-4"><label for="title">Title<span class="required-field-marker">*</span></label>
<div class="select-input-arrow">
    <input type="hidden" name="service_type" value="finance" />
    
    <select id="title" class="form-control" name="title" required>
<option selected="selected" value="">choose</option>
<option value="Mr">Mr</option>
<option value="Mrs">Mrs</option>
<option value="Miss">Miss</option>
<option value="Ms">Ms</option>
<option value="Dr">Dr</option>
</select></div>
</div>
<div class="form-group col-md-11"><label for="firstName">First Name<span class="required-field-marker">*</span></label> <input id="first_name" class="form-control" maxlength="75" name="first_name" type="text" value="" placeholder="John" required /></div>
<div class="form-group col-md-11"><label for="lastName">Last Name<span class="required-field-marker">*</span></label> <input id="last_name" class="form-control" maxlength="75" name="last_name" type="text" value="" placeholder="Doe" required/></div>
<div class="form-group col-md-11"><label for="address">Residential Address</label><textarea id="address" class="form-control" maxlength="1000" name="address" rows="4" placeholder="House#123, Street ABC"  ></textarea>(Optional)</div>
<div class="form-group col-md-11"><label for="suburb">Suburb/Town</label> <input id="suburb" class="form-control" maxlength="255" name="suburb" type="text" value="" placeholder="Doe" /> (Optional)</div>
<div class="form-group col-md-11"><label for="state">State<span class="required-field-marker">*</span></label>
<div class="select-input-arrow"><select id="state" class="form-control" name="state" required>
<option selected="selected" value="">choose</option>
<option value="Australian Capital Territory">Australian Capital Territory</option>
<option value="New South Wales">New South Wales</option>
<option value="Northern Territory">Northern Territory</option>
<option value="Queensland">Queensland</option>
<option value="South Australia">South Australia</option>
<option value="Tasmania">Tasmania</option>
<option value="Victoria">Victoria</option>
<option value="Western Australia">Western Australia</option>
</select></div>
</div>
<div class="form-group col-md-4"><label for="postcode">Postcode</label> <input id="postcode" class="form-control" maxlength="30" name="postcode" type="text" value="" placeholder="6700" /> (Optional)</div>
</div></fieldset>
<fieldset class=" col-md-6 contact-details-enquiry-form">
<h4>Contact Details: <small>You must supply at least one valid phone number home, work or mobile</small></h4>
<div class="row">
<div class="form-group col-md-11"><label for="homePhone">Home Phone</label> <input id="homePhone" class="form-control" maxlength="18" name="home_phone" type="tel" value="" placeholder="+123456789" /></div>
<div class="form-group col-md-11"><label for="workPhone">Work Phone</label> <input id="workPhone" class="form-control" maxlength="18" name="work_phone" type="tel" value="" placeholder="+123456789" /></div>
<div class="form-group col-md-11"><label for="mobilePhone">Mobile Phone</label> <input id="mobilePhone" class="form-control" maxlength="18" name="mobile_phone" type="tel" value="" placeholder="+123456789" /></div>
<div class="form-group col-md-11"><label for="emailAddress">Email Address</label> <input id="emailAddress" class="form-control" maxlength="75" name="email_address" type="email" value="" placeholder="john@example.com" /></div>
<div class="form-group col-md-11"><label for="confirmEmailAddress">Confirmation Email Address</label> <input id="confirmEmailAddress" class="form-control" maxlength="75" name="confirm_email_address" type="email" value="" placeholder="john@example.com" /></div>
</div></fieldset>
<fieldset class="col-md-6 clearfix finance-eligibility-enquiry-form">
<h4>Finance Eligibility:</h4>
<div class="row">
<div class="form-group col-md-11"><label for="employmentStatus">Employment Status<span class="required-field-marker">*</span></label>
<div class="select-input-arrow"><select id="employmentStatus" class="form-control" name="employment_status" required>
 <option selected="selected" value="">choose</option>       
<option value="Full Time">Full Time</option>
<option value="Part Time">Part Time</option>
<option value="Casual">Casual</option>
<option value="Self Employed">Self Employed</option>
</select></div>
</div>
<div class="form-group col-md-11"><label for="livingArrangement">Living Arrangements<span class="required-field-marker">*</span></label>
<div class="select-input-arrow"><select id="livingArrangement" class="form-control" name="living_arrangement" required>
<option selected="selected" value="">choose</option>
<option value="Own Home">Own Home</option>
<option value="Renting">Renting</option>
<option value="Boarding">Boarding</option>
<option value="Mortgage">Mortgage</option>
</select></div>
</div>
<div class="form-group col-md-11"><label for="productToPurchase">Product to be purchased<span class="required-field-marker">*</span></label>
<div class="select-input-arrow"><select id="productToPurchase" class="form-control" name="product_to_purchase" required>
<option selected="selected" value="">choose</option>
        <option value="Motor Vehicle">Motor Vehicle</option>
<option value="Motorbike">Motorbike</option>
<option value="Boat">Boat</option>
<option value="Caravan">Caravan</option>
<option value="Jet Ski">Jet Ski</option>
<option value="Home Theatre">Home Theatre</option>
<option value="Other">Other</option>
</select></div>
</div>
</div></fieldset>
<fieldset class=" col-md-6 clearfix response-enquiry-form">
<h4>Response:</h4>
<div class="row">
<div class="form-group col-md-11"><label for="contactTime">Preferred Contact time<span class="required-field-marker">*</span></label>
<div class="select-input-arrow"><select id="contactTime" class="form-control" name="contact_time" required>
        <option selected="selected" value="">choose</option>
<option  value="Business Hours">Business Hours</option>
<option value="After Hours">After Hours</option>
</select></div>
</div>
<div class="form-group col-md-11"><label for="contactMethod">Preferred contact method<span class="required-field-marker">*</span></label>
<div class="select-input-arrow"><select id="contactMethod" class="form-control" name="contact_method" required>
 <option selected="selected" value="">choose</option>       
<option value="Email">Email</option>
<option  value="Mobile Phone">Mobile Phone</option>
<option value="Work Phone">Work Phone</option>
<option value="Home Phone">Home Phone</option>
</select></div>
</div>
</div></fieldset>
<div class=" col-md-12"><input id="finance" class="btn btn-primary" name="finance" type="submit" value="Submit" /></div>
<div class="clearfix"></div>
</div>
</div>
</form>
    <div id="modal-overlay">
    <img id="text" src="<?php echo plugin_dir_url( __FILE__);  ?>img/spiffygif_32x32.gif" />
</div>