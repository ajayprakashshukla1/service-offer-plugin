<?php

/*
 *
 *  Plugin Name: Services
  Description: This plugin is for just for offering Services
  Author: Codeup
 */

class Services {

    public static function load_header() {
        wp_enqueue_script('Vallidate', plugin_dir_url(__FILE__) . 'js/jquery.validate.js');
    }
    
    public static function load_finance_js() {
        wp_enqueue_script('Custom_vallidate', plugin_dir_url(__FILE__) . 'js/finance.validate.js');
    }
    public static function load_trade_in_js() {
        wp_enqueue_script('Custom_vallidate', plugin_dir_url(__FILE__) . 'js/tradein.validate.js');
    }
    public static function load_warranty_js() {
        wp_enqueue_script('Custom_vallidate', plugin_dir_url(__FILE__) . 'js/warranty.validate.js');
    }

    public static function finance_services($atts) {
    self::load_header();
     self::load_finance_js();
        if (isset($_POST['finance'])) {

            unset($_POST['finance']);
            $subject = 'Finance';
            $success = self:: send_offer_detail_mail($_POST,$subject);
            if ($success == 1) {
                $success_message = "Enquiry sumbitted Successfully";
            } else {
                $error_message = "Something Went Wrong Please try again";
            }
            
        }

        //ob_start();
        require_once( plugin_dir_path(__FILE__) . 'finance.php' );
        die();
        //return ob_get_clean();
    }

    public static function trade_in_services($atts) {
    self::load_header();
     self::load_trade_in_js();
        if (isset($_POST['trade_in'])) {
            unset($_POST['trade_in']);
             $subject = 'Trade In';
            $success = self:: send_offer_detail_mail($_POST,$subject);
            if ($success == 1) {
                $success_message = "Enquiry sumbitted Successfully";
            } else {
                $error_message = "Something Went Wrong Please try again";
            }
        }

        //ob_start();
        require_once( plugin_dir_path(__FILE__) . 'trade_in.php' );
        die();
       // return ob_get_clean();
    }

    public static function warranty_services($atts) {
       self::load_header();
        self::load_warranty_js();
        if (isset($_POST['warranty_form'])) {

            unset($_POST['warranty_form']);
            $subject = 'Warranty';
            $success = self:: send_offer_detail_mail($_POST,$subject);
            if ($success == 1) {
                $success_message = "Enquiry sumbitted Successfully";
                
            } else {
                $error_message = "Something Went Wrong Please try again";
            }
        }

        //ob_start();
        require_once( plugin_dir_path(__FILE__) . 'warranty.php' );
        die();
        //return ob_get_clean();
    }

    public static function send_offer_detail_mail($posted_data,$subject) {
        
       // unset($posted_data['action']);
        $subject = $_POST['subject'];
        parse_str($_POST['data'], $posted_data);
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $message = self::submited_offer_data_table($posted_data);
        $admin_email = get_option('admin_email');
        
        $success = wp_mail($admin_email, $subject.' Enquiry', $message,$headers);
        //return $error;
        echo $success;
        die();
    }

    public static function submited_offer_data_table($posted_data) {
        $html = '';
        $html .= '<table width="100%">';
        $html .='<tr><th>Field</th></th><th>Value</th></tr>';
        foreach ($posted_data as $key => $value) {
            
            if (strpos($key, '_') !== false) {
                $correct_label = self::key_lable($key);
            }else{
                $correct_label = ucfirst($key);
            }
            $html .='<tr><td>' . $correct_label . '</td><td>' . $value . '</td></tr>';
        }
        $html .= '</table>';
        return $html;
    }
    
     public static function key_lable($key) {
         
          $key_array = explode('_', $key);
                $new_temp_key = '';
                $new_temp_key_array = array();
                
                foreach($key_array as $key_value){
                    $new_temp_key_array[]= ucfirst($key_value);
                }
                
                $correct_label = implode(' ',$new_temp_key_array);
                return $correct_label;
     }

}

add_shortcode('finance', array('Services', 'finance_services'));
add_shortcode('trade_in', array('Services', 'trade_in_services'));
add_shortcode('warranty', array('Services', 'warranty_services'));



add_action('wp_ajax_send_offer_detail_mail',array('Services', 'send_offer_detail_mail'));
add_action('wp_ajax_nopriv_send_offer_detail_mail',array('Services', 'send_offer_detail_mail'));
